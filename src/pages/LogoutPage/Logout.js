import { logout } from "../../redux/actions/LoginActions";
import history from "../../history";
import store from "../../redux/store";

export default function Logout() {
    store.dispatch(logout());
    history.push("/login");
    return null;
}