import React from 'react';
import { Link } from 'react-router-dom';

import './DetailPharmacy.css';

export default function DetailPharmacy(props) {
    if (props.pharmacy !== undefined) {
        const { name, address, city, postalCode } = props.pharmacy;
        return (
            <div className="DetailPharmacy">
                <h3>{ name }</h3>
                <p>{ address + " - " + postalCode + " " + city }</p>
                <Link to="/catalog">Retour à la liste des pharmacies</Link>
            </div>
        );
    } else {
        return (
            <p>Pharmacie inexistante</p>
        )
    }
}