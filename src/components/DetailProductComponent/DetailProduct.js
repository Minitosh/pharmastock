import React from 'react';
import { Link } from 'react-router-dom';

import './DetailProduct.css';

export default function DetailProduct(props) {
    if (props.product !== undefined) {
        const { name, price } = props.product;
        return (
            <div className="DetailProduct">
                <h3>{ name }</h3>
                <p>{ price }</p>
                <Link to="/catalog">Retour à la liste des produits</Link>
            </div>
        );
    } else {
        return (
            <p>Produit inexistant</p>
        )
    }
}